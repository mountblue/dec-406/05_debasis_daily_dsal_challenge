/**
 * LINKED LIST
 */
class Node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class LinkedList{
    constructor(){
        this.head = null
        this.length = 0
    }

    add(data){
        let node = new Node(data)
        if(this.head == null){
            this.head = node
        }else{
            let curr = this.head
            while(curr.next){
                curr = curr.next
            }
            curr.next = node
        }
        this.length++
    }
    remove(el){
        let currNode = this.head
        let prevNode;
        if(this.head == null) return undefined;
        if(currNode.data == el){
            this.head = currNode.next;
        }else{
            while(currNode.data !== el){
                prevNode = currNode
                currNode = currNode.next
            }
            prevNode.next = currNode.next
        }
        this.length--
    }

    size(){
        return this.length
    }

    elmentAt(index){
        let currNode = this.head
        let prevNode;
        let nextNode;
        let count = 0
        if( index > this.length ) return;
        while(count < index){
            count++
            prevNode = currNode
            currNode = currNode.next
            nextNode = currNode.next
        }
        return { prevNode, currNode, nextNode, count }
    }
    
    insertAt(index,el){
        let node = new Node(el)
        let currNode = this.head
        let prevNode;
        let count = 0;
        if(index > this.length){
            return false
        }
        if(index == 0){
            node.next = currNode
            this.head = node
        }else{
            while(count < index){
                count++
                prevNode = currNode
                currNode = currNode.next
            }
            node.next = currNode
            prevNode.next = node
        }
        this.length++
    }

    removeAt(index){
        let currNode = this.head
        let prevNode;
        let count = 0;
        if(index >= this.length || index < 0){
            return null
        }
        if(index == 0){
            this.head = currNode.next
        }else{
            while(count < index){
                count++
                prevNode = currNode
                currNode = currNode.next
            }
            prevNode.next = currNode.next
        }
        this.length--
        return currNode.data
    }


}




let data = {
    Node, LinkedList
}

module.exports = data;