let MakeGraph = () => {
    let graph = {}

    graph.contains = (node) => {
        return Boolean(graph[node])
    }

    graph.addVertex = (node) => {
        if(!graph.contains(node)){
            graph[node] = {edges:{}}
        }
    }

    graph.addEdge = (startnode, endnode) => {
        if(graph.contains(startnode) && graph.contains(endnode)){
            graph[startnode].edges[endnode] = true
            graph[endnode].edges[startnode] = true
        }
    }   

    graph.hasEdge = (node) => {
        if(graph.contains(node)) {
            return Boolean(Object.keys(graph[node].edges).length)
        }
    }

    graph.removeVertex = (node) => {
        if(graph.contains(node)) {
            for(let connectedNode in graph[node].edges) {
                graph.removeEdge(node, connectedNode)
            }
            delete graph[node]
        }

    }

    graph.removeEdge = (startnode, endnode) => {
        if(graph.contains(startnode) && graph.contains(endnode)){
            delete graph[startnode].edges[endnode]
            delete graph[endnode].edges[startnode]
        }
    }   

    return graph
}

let mbBootCamp = MakeGraph()
mbBootCamp.addVertex("debasis")
mbBootCamp.addVertex("prajwal")
mbBootCamp.addVertex("soumya")
console.log(mbBootCamp.contains(""))
mbBootCamp.addEdge("debasis", "soumya")
console.log(mbBootCamp)
mbBootCamp.removeEdge("debasis", "soumya")
mbBootCamp.removeVertex("prajwal")
console.log(mbBootCamp)
console.log(mbBootCamp.hasEdge("debasis"))