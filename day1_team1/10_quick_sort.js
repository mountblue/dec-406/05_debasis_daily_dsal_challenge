

// Q10 sort the array using quick sort algorithm
function quick_sort(l) {
    if( l.length <= 1)  return l;

    let left_value = []
    let right_value = []
    let pivot = l[0]

    for(let value of l.slice(1)){
        if(value <= pivot){
            left_value.append(value)
        } else {
            right_value.append(value)
        }
    }

    return quick_sort(left_value) + [pivot] + quick_sort(right_value)
}



