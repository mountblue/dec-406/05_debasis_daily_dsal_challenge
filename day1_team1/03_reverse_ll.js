const { Node, LinkedList } = require("../data/linkedlist");

// Q3 reverse a linked list
function reverseLinkedList(ll) {
    let stack = []
    let curr = ll.head
    while(ll.length) {
        stack.push(curr.data)
        curr = curr.next
        ll.length--
    }
    let reversedLL = new LinkedList()
    while(stack.length) {
        reversedLL.add(stack.pop())
    }
    return reversedLL
}
// console.log(reverseLinkedList(LL))
