const { Node, LinkedList } = require("../data/linkedlist");

// Q2 swap nodes of the linked list
LinkedList.prototype.swapNodes = (index1, index2) => {
    let firstNode = this.elmentAt(index1)
    let secondNode = this.elmentAt(index2)
    firstNode.prevNode.next = secondNode.currNode
    firstNode.prevNode.next.next = secondNode.prevNode
    secondNode.prevNode.next = firstNode.currNode
    secondNode.prevNode.next.next = secondNode.nextNode
    return this
}

// console.log(LL.elmentAt(2))
LL.swapNodes(2,3)
// console.log(LL.elmentAt(2))