// Q7 move spaces to front of string in single traversal

function addSpacesToFront(string){
    let chars = "";
    let countSpace = 0
    for(let char of string) {
        if(char !== " "){
            chars += char
        } else {
            countSpace += 1
        }
    } 
    return " ".repeat(countSpace) + chars
}
const getvalue = addSpacesToFront("hello there mr")
// console.log(getvalue)



