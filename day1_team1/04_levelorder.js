const { BinaryTree, Node:BNode } = require("../data/BinaryTree")
//Q4  binary tree insertion - level order
let bt = new BinaryTree()
bt.root =  new BNode(2)
bt.root.left =  new BNode(4)
bt.root.right =  new BNode(6)
let temp = bt.root

function inorder(temp) {
    if(temp === null) {
        return;
    }
    inorder(temp.left)
    console.log(temp.data)
    inorder(temp.right)
}
inorder(temp)

function levelOrderInsertion(node , data) {
        let q = []
        q.push(node); 
    
        while (!q.length) { 
            node = q[0]; 
            q.pop(); 
       
            if (node.left == null) { 
                node.left = new BNode(data); 
                break; 
            } else
                q.push(node.left); 
       
            if (node.right == null) { 
                node.right = new Node(data); 
                break; 
            } else
                q.push(node.right); 
        } 
    
}
