//Q8 design a url shortner using hashing

function urlSohrtner(url) {
    let randomSlice = Math.floor(Math.random() * (0 + url.length/2) + 0);
    let shortUrl = [...url].slice(randomSlice,4).join("")
    return shortUrl === "" ? [...url].slice(0,6).join("") : shortUrl.repeat(2)
}
// console.log(urlSohrtner("hello/hhs/hhja"))
