// Q6 sum of 2 very large numbers
function sums(arg1, arg2) {
	let sum = "";
    let r = 0;
    arg1 = String(arg1)
    arg2 = String(arg2)
	let a1, a2, i;
	if (arg1.length < arg2.length) {
		a1 = arg1;
		a2 = arg2;
	} else {
		a1 = arg2;
		a2 = arg1;
	}
	a1 = [...a1].reverse();
	a2 = [...a2].reverse();
	for (i = 0; i < a2.length; i++) {
		let t = ((i < a1.length) ? parseInt(a1[i]) : 0) + parseInt(a2[i]) + r;
		sum += t % 10;
		r = t < 10 ? 0 : Math.floor(t / 10);
	}
	if (r > 0) sum += r;
    
    sum = sum.split("").reverse();
    
	while (sum[0] == "0")
		sum.shift();

	return sum.length > 0 ? sum.join("") : "0";
}
