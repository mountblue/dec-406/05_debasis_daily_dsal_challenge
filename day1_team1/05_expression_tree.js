//Q5  solve an expression using expression tree
function expressionTree(root) { 
    if(root === null)  return 0;
    
    if(root.left === null && root.right === null){
        return parseInt(root.data)
    }
    let left_sum = expressionTree(root.left)  
    let right_sum = expressionTree(root.right) 
    if(root.data === '+') {
        return left_sum + right_sum 
    } else if (root.data == '-') { 
        return left_sum - right_sum
    } else if (root.data == '*') { 
        return left_sum * right_sum
    } else {
        return left_sum / right_sum 
    }
}

