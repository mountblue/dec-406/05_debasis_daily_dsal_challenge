// Q9  randomize array
function randomize (arr, n) { 
    for(let i = n - 1; i >= 0; i--)  { 
        let j = Math.floor(Math.random() * (0, i+1) + 0)  
        let tmp = arr[i]; 
        arr[i] = arr[j]; 
        arr[j] = tmp; 
    } 
    for(i = 0; i < n; i++) 
        console.log(`${arr[i]}`); 
} 
