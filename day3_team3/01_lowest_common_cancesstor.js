const { BinaryTree, Node }  = require("../data/BinaryTree");

/**
 * 
 * @param {*} node 
 * @param {*} data 
 * @param {*} track 
 */
function searchThePath(node, data, track = []){
	if(node === null){
		return false
	}
	track.push(node)
	if(node.data === data) {
		return true
	}

	if((node.left !== null &&  searchThePath(node.left, data, track) )||
	(node.right !== null && searchThePath(node.right, data, track))){
	   return true
   }
   track.pop()
   return false;
}

let bt = new BinaryTree()
bt.root = new Node(2)
bt.root.left = new Node(3)
bt.root.right =  new Node(4)
bt.root.right.left = new Node(5)
bt.root.left.right = new Node(6)
bt.root.right.right =  new Node(7)

/**
 * 
 * @param {*} root 
 * @param {*} node1 
 * @param {*} node2 
 * @param {*} cb 
 */

function findLowestCommonAncesstor(root,node1,node2,cb) {
    let track1 = [], track2 = []
    cb(root,node1, track1)
    cb(root, node2, track2) 
    const common = track1.filter( o => track2.some(({data}) => o.data === data))
    return common[common.length-1].data
}

console.log(findLowestCommonAncesstor(bt.root,5,6, searchThePath))