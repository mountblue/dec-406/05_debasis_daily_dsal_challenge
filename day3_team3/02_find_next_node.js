const { BinaryTree, Node }  = require("../data/BinaryTree");

let bt = new BinaryTree()
bt.root = new Node(2)
bt.root.left = new Node(3)
bt.root.right =  new Node(4)
bt.root.right.left = new Node(5)
bt.root.left.right = new Node(6)
bt.root.right.right =  new Node(7)

/**
 * 
 * @param {*} tree 
 * @param {*} node 
 */

function findNextNode(tree,node){
    const level = tree.levelOrder()
    if(level.includes(node)) {
        return level[level.indexOf(node) + 1] === undefined  || level[level.indexOf(node) + 1] === undefined ? null : level[level.indexOf(node) + 1];
    }
}

console.log(findNextNode(bt,3))