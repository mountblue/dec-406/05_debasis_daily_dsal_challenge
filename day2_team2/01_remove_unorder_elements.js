const { LinkedList, Node } = require("../data/linkedlist");
// Q1. Remove any “unordered” elments (i.e. leave the list in non-decreasing order)

class SuperLinkedList extends LinkedList {
    constructor(){
        super()
    }

    elmentAt(index){
       return super.elmentAt(index)
    }

    reversePrint(node){
        if(node === null){
            return;
        }
        this.reversePrint(node.next)
        console.log(node.data)
        
    }
    
}

function removeUnorderElements(ll) {
    let curr = ll.head
    if(curr === null) return undefined;
    let maxValue = ll.head.data;
    let prev = null
    while(curr) {
        let currData = curr.data
        if(currData < maxValue) {
            prev.next = curr.next
        } else {
            maxValue = curr.next.data
            prev = curr
        }
        curr = curr.next
    }
}

module.exports = {
    SuperLinkedList
}