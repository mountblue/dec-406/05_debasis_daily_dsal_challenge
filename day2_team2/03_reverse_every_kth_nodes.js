const { SuperLinkedList } = require("./01_remove_unorder_elements")
// Q3  Given a linked list and a number k. Reverse every k nodes in the list.

function reverseTheList(ll){
    let prev = null;
    let curr = ll.head
    while(curr){
        let next = curr.next
        curr.next = prev
        prev = curr
        curr = next
    }
    ll.head = prev
    return ll
}

function reverseThekthElement(ll, number){
    let list = new SuperLinkedList()
    let count = 0
    let curr = ll.head
    while(count <= number) {
        count++
        list.add(curr.data)
        curr = curr.next
    }
    const node = ll.elmentAt(number).nextNode;
    const reversed = reverseTheList(list)
    let start =  reversed.head
    while(start.next) {
        start = start.next
    }
    start.next = node
    return reversed
}



let ll =  new SuperLinkedList()
ll.add(1)
ll.add(2)
ll.add(3)
ll.add(4)
reverseThekthElement(ll, 2)
