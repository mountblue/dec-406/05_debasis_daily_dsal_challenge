const { LinkedList, Node } = require("../data/linkedlist");

// Q2. Given a linked list and two integers M and N. Traverse the linked list such that you retain M nodes then delete next N nodes, continue the same until end of the linked list.

function skipMnodesAndDeleteNnodes(ll, m, n ){
    let curr = ll.head; 
    let count, t;
  
    while (curr) {  
        for (count = 1; count< m  && curr !== null ; count++) 
            curr = curr.next; 
        if (curr === null) 
            return; 
        t = curr.next; 
        for (count = 1; count <= n && t!= null; count++) { 
            let temp = t; 
            t = t.next; 
            delete temp; 
        } 
        curr.next = t; 
        curr = t; 
    } 
}
