const { LinkedList, Node } = require("../data/linkedlist");


LinkedList.prototype.swapEveryTwoNodes = () => {
    if ( this.head === null ) return;
    let curr = this.head
    while( curr && curr.next ) {
        let temp = curr
        curr = curr.next
        curr.next = temp 

        curr = curr.next.next
    }
    return this.head
}