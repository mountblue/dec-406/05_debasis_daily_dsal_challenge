const { LinkedList, Node } = requier("../data/linkedlist.js")


LinkedList.prototype.getNth = (index) => {
    let curr = this.head
    let count = 0 
    if ( index > this.length ) return;
    while( count < index ) {
        count++
        curr = curr.next
    }
    return curr.data
}